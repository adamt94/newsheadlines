import { connect } from 'react-redux'
import Dashboard from '../components/Dashboard';
import * as actionCreators from "../actions/Headlines"

const mapStateToProps = (state, ownProps) => ({
  ...state.headlineState
})


export default connect(
  mapStateToProps,
  actionCreators
)(Dashboard)