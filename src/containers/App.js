import { connect } from 'react-redux'
import App from '../components/App';
import * as actionCreators from "../actions/Headlines"

const mapStateToProps = (state, ownProps) => ({
  ...state
})


export default connect(
  mapStateToProps,
  actionCreators
)(App)