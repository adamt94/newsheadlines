import reducer from '../../reducers/headlines'

describe('Headlines reducer', () => {
    it('should return the initial state', () => {
      expect(reducer(undefined, {})).toEqual(
        {
         headlines:[],
         isLoading:false
        }
      )
    })
    it('should handle GET_HEADLINES', () => {
        expect(
          reducer([], {
            type: "GET_HEADLINES",
            payload: {status:"ok",articles:[]},
            isLoading: true
          })
        ).toEqual(
            {"headlines": {status:"ok",articles:[]}, isLoading: true}
        )
    }
)})
    