import * as actions from '../../actions/Headlines'

describe('actions', () => {
    it('should create an action to get headlines', () => {
      const data = {
        headlines:{}
    }
      const expectedAction = {
        type: "GET_HEADLINES",
        payload: data,
        isLoading: false
      }
      expect(actions.getHeadlines(data)).toEqual(expectedAction)
    })
    it('should create an action to fetch data', () => {
     
        const expectedAction = {
          type: "FETCHING_DATA",
          isLoading: true
        }
       
      })
  })