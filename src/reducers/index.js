import { combineReducers } from 'redux'

import headlineState from './headlines'

export default combineReducers({
    headlineState
  })