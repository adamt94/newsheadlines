const initialState = {
    headlines:[],
    isLoading: false
  }
  const headlineReducer = (state = initialState, action) => {
      console.log(action)
      switch (action.type) {
        case 'GET_HEADLINES':
          return {
            ...state,
            headlines: action.payload,
            isLoading: action.isLoading
  
          }
          case 'FETCHING_DATA':
          return {
            ...state,
            isLoading: action.isLoading
          }
        default:
          return state
      }
    }
    
    export default headlineReducer;