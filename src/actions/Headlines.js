import axios from "axios";


//todo add dispatch for loading and error message
export function fetchHeadlines(countryCode){
    return(dispatch)=>{
        dispatch(setLoading(true))
        return axios.get("http://newsapi.org/v2/top-headlines?country=" + countryCode + "&pageSize=5&apiKey="+process.env.REACT_APP_NEWS_API_KEY).then((response)=>{
            console.log(response);
            dispatch(getHeadlines(response.data));
        }).catch(error => {
          console.log(error.response)
      });
    }
}
export default async term => {
    const response = await axios.get("https://api.unsplash.com/search/photos", {
      params: {
        client_id: process.env.REACT_APP_UNSPLASH_TOKEN,
        query: term
      }
    });
  
    return response.data.results;
  };

export const setLoading = (value) =>({
    type: 'FETCHING_DATA',
    isLoading: value
})
export const getHeadlines= (headlines) => ({
    type: 'GET_HEADLINES',
    payload: headlines,
    isLoading: false
  })

