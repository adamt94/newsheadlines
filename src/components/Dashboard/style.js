import styled from 'styled-components'
import { secondary} from '../../util/colors'

export const Wrapper = styled.section`
padding: 1rem;
color: ${secondary}
`;


export const Content = styled.section`
padding: 2.2rem;
`;

export const GridLayout = styled.section`
display: grid;

grid-template-columns: repeat(auto-fit, minmax(500px, 1fr));
@media (max-width: 768px) {
    grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
  }
`;

export const CardButtons = styled.section`
float: right;
`;

export const ProgressContainer = styled.section`
width: 100%;
text-align: center;

`;