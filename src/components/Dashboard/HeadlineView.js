import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import {Wrapper} from './style'
import clsx from 'clsx';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import CardActions from '@material-ui/core/CardActions';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Collapse from '@material-ui/core/Collapse';
import Button from '@material-ui/core/Button';



const useStyles = makeStyles({
    root: {
      maxWidth: 475,
    },
    media: {
      height: 300,
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
      },
      expandOpen: {
        transform: 'rotate(180deg)',
      },
      avatar: {
      },
  });


export default function HeadLineView(props) {
    const classes = useStyles()
    const { urlToImage, title, author, content, description, url } = props.articleData;
    const [expanded, setExpanded] = React.useState(false);

    const handleExpandClick = () => {
        setExpanded(!expanded);
    };
    return (

        <Wrapper>
         <Card className={classes.root}>
            <CardMedia
                    className={classes.media}
                    image={urlToImage}
                    title="Contemplative Reptile"
                />
            <CardContent>
            <Typography gutterBottom color="textPrimary" variant="h5" component="h2">
            {title}
          </Typography>
                <Typography gutterBottom color="textSecondary"  component="h3">
                        {author}
                </Typography>
               
                <Typography variant="body2" color="textSecondary" component="p">
                        {description}
                </Typography>
            </CardContent>
        <CardActions disableSpacing>
        <Button href={url} size="small" color="primary">
          Source
        </Button>
        {content && <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </IconButton>}
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          
          <Typography paragraph>
           {content}
          </Typography>
          
          
        </CardContent>
      </Collapse>
        </Card>
        </Wrapper>
    );



  }


