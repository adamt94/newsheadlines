import React,{ useEffect} from 'react';
import {GridLayout, ProgressContainer} from './style';

import CircularProgress from '@material-ui/core/CircularProgress';
import HeadlineView from './HeadlineView'

//Dashboard dispalys grid of headline containers
//Dashboard component rendered in Tabpanel.js
export default function Dashboard({isLoading,headlines, country, fetchHeadlines}) {

  useEffect(() => {
    fetchHeadlines(country.code);
  },[country.code, fetchHeadlines]);
  
  const  items =()=>{
    return (
        <GridLayout>
            { headlines.articles? headlines.articles.map((row,index) => (  
                <HeadlineView key = {index} articleData = {row}/>
    
            )):("")}
        </GridLayout>
      )
  }

    return (
    <div>
    {isLoading? 
        (<ProgressContainer><CircularProgress /></ProgressContainer>):
        (items(headlines))
    }
    </div>



    );
  }

