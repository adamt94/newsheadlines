import React from 'react';
import './App.css';
import {theme} from '../../util/colors'
import { MuiThemeProvider } from '@material-ui/core/styles';
import NavBar from '../NavBar'


function App(props) {
  return (

    
    <MuiThemeProvider theme={theme}>
      <NavBar/>
    </MuiThemeProvider>
  );
}

export default App;
