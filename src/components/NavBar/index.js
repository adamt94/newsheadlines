import React from 'react';
import FullWidthTabs from './TabPanel';
import {Nav} from './style'
import {countries} from '../../util/variables'

//Navbar renders tabs used from survey data
export default function NavBar() {
  
    return (
     <Nav>
        <h1>Headline News</h1>
        <FullWidthTabs data={countries}/>


        </Nav>
    );
  }