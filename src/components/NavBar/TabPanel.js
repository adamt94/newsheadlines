import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { background } from '../../util/colors';
import Dashboard from '../../containers/Dashboard'

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: background,
  },
}));

export default function SimpleTabs(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const {data} = props;
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static" elevation={0}>
        <Tabs value={value} onChange={handleChange} aria-label="simple tabs example"  variant="scrollable"
          scrollButtons="auto">
        {data ? data.map((row,index)=>( 
            <Tab key={index} label={row.country} />

        )): ( "No Data Available")} 
        {
        }
        </Tabs>
      </AppBar>

       {data ? data.map((row,index) =>( 
          <TabPanel key={index} value={value} index={index}>
            <Dashboard country ={row}/>
        </TabPanel>
        )): ( "No Data Available")} 
        {
        }
    </div>
  );
}