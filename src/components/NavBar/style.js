import {primary, secondary} from '../../util/colors'
import styled from 'styled-components'



export const Nav = styled.section`
 background: ${primary};
 color: ${secondary}
`;