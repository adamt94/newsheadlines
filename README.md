## Headline news
App shows the top 5 headlines from a list of countries (Uk,France, US, India and Australia)
## Available Scripts

In the project directory, you can run:

### `npm install`

### `npm run start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm run test`

Launches the test runner in the interactive watch mode.
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.


## Packages used
- material ui
- jest
- axios
- redux

